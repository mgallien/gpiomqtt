
all: gpiomqtt

gpiomqtt: main.cpp
	${CXX} -o $@ $< -lgpiod -lmosquitto

clean:
	${RM} gpiomqtt

