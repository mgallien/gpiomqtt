#include <iostream>
#include <gpiod.h>
#include <mosquitto.h>
#include <time.h>

int main(int argc, char*argv[])
{
    auto chipname = "gpiochip0";
    struct timespec sleepDelay {2, 0};
    auto clientName = "switch-dev";

    mosquitto_lib_init();

    auto mosquittoHandle = mosquitto_new(clientName, true, nullptr);
    std::cout << "gpiomqtt: mosquittoHandle: " << mosquittoHandle << std::endl;

    auto chip = gpiod_chip_open_by_name(chipname);
    std::cout << "gpiomqtt: chip: " << chip << std::endl;

    auto relay1Line = gpiod_chip_get_line(chip, 14);
    std::cout << "gpiomqtt: relay1Line: " << relay1Line << std::endl;

    auto relay2Line = gpiod_chip_get_line(chip, 12);
    std::cout << "gpiomqtt: relay2Line: " << relay2Line << std::endl;

    auto result = gpiod_line_request_output(relay1Line, "example1", 0);
    std::cout << "gpiomqtt: gpiod_line_request_output: relay1Line: " << result << std::endl;

    result = gpiod_line_request_output(relay2Line, "example1", 0);
    std::cout << "gpiomqtt: gpiod_line_request_output: relay2Line: " << result << std::endl;

    result = gpiod_line_set_value(relay1Line, 0);
    std::cout << "gpiomqtt: gpiod_line_set_value: 0: relay1Line: " << result << std::endl;

    result = gpiod_line_set_value(relay2Line, 0);
    std::cout << "gpiomqtt: gpiod_line_set_value: 0: relay2Line: " << result << std::endl;

    nanosleep(&sleepDelay, nullptr);

    result = gpiod_line_set_value(relay1Line, 1);
    std::cout << "gpiomqtt: gpiod_line_set_value: 1: relay1Line: " << result << std::endl;

    result = gpiod_line_set_value(relay2Line, 1);
    std::cout << "gpiomqtt: gpiod_line_set_value: 1: relay2Line: " << result << std::endl;

    nanosleep(&sleepDelay, nullptr);

    result = gpiod_line_set_value(relay1Line, 0);
    std::cout << "gpiomqtt: gpiod_line_set_value: 0: relay1Line: " << result << std::endl;

    result = gpiod_line_set_value(relay2Line, 0);
    std::cout << "gpiomqtt: gpiod_line_set_value: 0: relay2Line: " << result << std::endl;

    gpiod_line_release(relay1Line);
    gpiod_line_release(relay2Line);
    gpiod_chip_close(chip);

    mosquitto_lib_cleanup();

    return 0;
}
